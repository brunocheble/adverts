import { Router } from 'express';

import ServiceController from './app/controllers/ServiceController';
import FilterController from './app/controllers/FilterController';

const routes = new Router();

routes.post('/services', ServiceController.store);
routes.get('/start', ServiceController.start);

routes.post('/filters', FilterController.store);

export default routes;
