import Filter from '../models/Filter';

class FilterController {
    async store(req, res) {
        const filter = await Filter.create(req.body);
        return res.json(filter);
    }
}

export default new FilterController();
