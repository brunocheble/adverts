import Service from '../models/Service';
import Filter from '../models/Filter';
import Site from '../models/Site';
import Email from '../models/Email';

class ServiceController {
    async store(req, res) {
        const service = await Service.create(req.body);
        return res.json(service);
    }

    async start(req, res) {
        const services = await Service.find();

        services.map(async service => {
            const filters = await Filter.find({
                _serviceId: service.id,
            });

            setInterval(async () => {
                // Generate Interval
                console.log(`Intervalo ${service.interval} segundos.`);
                console.log(`Emails: ${service.emails}`);
                console.log(new Date().toLocaleString());

                // Para cada serviço, busca seus respetivos filtros;
                filters.map(async filter => {
                    const response = await Site.request(filter);

                    Email.send(
                        service.id,
                        service.emails,
                        response.title,
                        response.adverts
                    );
                });
            }, service.interval * 1000);
        });

        return res.json(services);
    }
}

export default new ServiceController();
