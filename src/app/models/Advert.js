import mongoose from 'mongoose';

const AdvertSchema = new mongoose.Schema(
    {
        link: String,
        last_price: Number,
        _serviceId: mongoose.Schema.Types.ObjectId,
    },
    {
        timestamps: true,
    }
);

export default mongoose.model('Advert', AdvertSchema);
