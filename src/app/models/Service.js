import mongoose from 'mongoose';

const ServiceSchema = new mongoose.Schema(
    {
        emails: String,
        interval: {
            type: Number,
            default: 0,
        },
    },
    {
        timestamps: true,
    }
);

export default mongoose.model('Service', ServiceSchema);
