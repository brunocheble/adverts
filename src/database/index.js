import mongoose from 'mongoose';

class Database {
    constructor() {
        this.init();
    }

    init() {
        mongoose.connect(
            'mongodb+srv://adverts:adverts@cluster0-azpu1.mongodb.net/test?retryWrites=true&w=majority',
            {
                useNewUrlParser: true,
            }
        );
    }
}

export default new Database();
